## Symrise HR Deployment Script

Instance : PRODUCTION

### Deployment Script

```
— BEGIN SCRIPT —

<script type="text/javascript">

"coming soon"

</script>

—- END of SCRIPT —

```

### Instructions to deploy

- Copy the code from above between the BEGIN and END of script
- Paste in the footer or header of the desired html/web page.
- Deploy the updated page to domain hosting

Your bot should be able to start on the right bottom of the page.

Happy Interacting!
